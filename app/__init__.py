from flask import Flask
from app.view import init_view
from app.controller import init_controller
from app.extensions import database
from app.extensions import serializer
from app.extensions import api_documentation
from config import config
from app.shell_context import register_shell_context


def create_app(config_name="default"):
    app = Flask(__name__, static_folder="./view/static", static_url_path="/")
    app.config.from_object(config[config_name])
    init_view(app)
    init_controller(app)

    serializer.init_app(app)
    database.init_app(app)
    with app.app_context():
        database.db.create_all()

    register_shell_context(app)

    api_documentation.init_app(app)

    return app
