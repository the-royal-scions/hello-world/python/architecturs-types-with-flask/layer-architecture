from flask import Blueprint
from .comments import CommentsResource
from .comment import CommentResource

bp = Blueprint("controller", __name__, url_prefix="/api")

bp.add_url_rule(
    "/comments",
    view_func=CommentsResource.as_view("comments"))

bp.add_url_rule(
    "/comment",
    view_func=CommentResource.as_view("comment"))


def init_controller(app):
    app.register_blueprint(bp)
