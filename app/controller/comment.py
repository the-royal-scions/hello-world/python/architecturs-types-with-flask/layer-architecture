from flask import request, jsonify
from flask.views import MethodView
from marshmallow.exceptions import ValidationError
from app.utils.exceptions import ObjectNotFound, ModelDoesntHaveAttr
from app.service.comments import CommentsService


class CommentResource(MethodView):
    @classmethod
    def get(cls):
        """
        Get comment by uuid
        ---
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - where
              properties:
                where:
                  type: object
        responses:
          200:
            description: Success to get the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        comment_service = CommentsService()

        where = request.args
        if not where:
            return {"message": "Missing query string"}, 400

        try:
            comment = comment_service.filter_by(**where)
        except ModelDoesntHaveAttr as error:
            return {"message": str(error)}, 400

        if not comment:
            return {"message": "Comment not found"}, 400

        return jsonify(comment), 200

    @classmethod
    def post(cls):
        """
        Create a comment
        ---
        consumes:
            - application/json
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - comment
              properties:
                comment:
                  type: string
        responses:
          200:
            description: Success to create the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        comment_service = CommentsService()
        request_json = request.get_json()
        if not request_json:
            return {"message": "A body with json is expected"}, 400

        try:
            comment = comment_service.create(**request_json)
            return jsonify(comment), 201
        except ValidationError as validation_error:
            return {"message": f"Data validation error. {validation_error}"}, 400
        except:
            return {"message": "Internal server error"}, 500

    @classmethod
    def put(cls):
        """
        Update a comment by uuid
        ---
        consumes:
            - application/json
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - payload
                  where
              properties:
                payload:
                  type: object
                  properties:
                    comment:
                      type: string
                where:
                  type: object
        responses:
          200:
            description: Success to update the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        comment_service = CommentsService()

        request_json = request.get_json()
        if not request_json:
            return {"message": "A body with json is expected."}, 400

        where = request_json.get("where")
        if not where:
            return {"message": "Missing where in the request body."}, 400

        payload = request_json.get("payload")
        if not payload:
            return {"message": "Missing payload in the request body."}, 400

        try:
            comment = comment_service.filter_by(**where)
        except ModelDoesntHaveAttr as error:
            return {"message": str(error)}, 400

        if not comment:
            return {"message": "Comment not found"}, 400

        try:
            comment_service.create(**payload)
            comment_service.delete(where)
            return {"message": "Comment updated"}, 200
        except ValidationError as validation_error:
            return {"message": f"Data validation error. {validation_error}"}, 400
        except:
            return {"message": "Internal server error"}, 500

    @classmethod
    def patch(cls):
        """
        Update a comment by uuid
        ---
        consumes:
            - application/json
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - payload
                  where
              properties:
                payload:
                  type: object
                  properties:
                    comment:
                      type: string
                where:
                  type: object
        responses:
          200:
            description: Success to update the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        comment_service = CommentsService()

        request_json = request.get_json()
        if not request_json:
            return {"message": "A body with json is expected."}, 400

        where = request_json.get("where")
        if not where:
            return {"message": "Missing where in the request body."}, 400

        payload = request_json.get("payload")
        if not payload:
            return {"message": "Missing payload in the request body."}, 400

        try:
            comment_service.update(where, **payload)
            return {"message": "Comment updated"}, 200
        except ObjectNotFound:
            return {"message": "Comment not found"}, 400
        except ModelDoesntHaveAttr as error:
            return {"message": str(error)}, 400
        except ValidationError as validation_error:
            return {"message": f"Data validation error. {validation_error}"}, 400
        except:
            return {"message": "Internal server error"}, 500

    @classmethod
    def delete(cls):
        """
        Delete a comment by some atribute
        ---
        parameters:
          - in: query
            name: uuid
            type: string
            required: false
          - in: query
            name: comment
            type: string
            required: false

        responses:
          200:
            description: Success to delete a comment by uuid
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        comment_service = CommentsService()

        where = request.args
        if not where:
            return {"message": "Missing query string"}, 400

        try:
            comment_service.delete(where)
            return {"message": "Comment deleted."}, 200
        except ObjectNotFound:
            return {"message": "Comment not found"}, 400
        except ModelDoesntHaveAttr as error:
            return {"message": str(error)}, 400
        except:
            return {"message": "Internal server error"}, 500
