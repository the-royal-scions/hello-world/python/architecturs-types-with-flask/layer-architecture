from flask.views import MethodView
from flask import jsonify
from app.service.comments import CommentsService


class CommentsResource(MethodView):
    @classmethod
    def get(cls):
        """
        Get all comments in database
        ---
        tags:
            - comments
        responses:
          200:
            description: Success in get comments 
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        comment_service = CommentsService()
        comments = comment_service.get_all()
        return jsonify(comments), 200
