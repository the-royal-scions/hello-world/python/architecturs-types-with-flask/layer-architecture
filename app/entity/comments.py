import uuid
from app.extensions.database import db


class CommentsModel(db.Model):
    __tablename__ = "comments"

    uuid = db.Column(db.String(36), primary_key=True)
    comment = db.Column(db.String(255), nullable=False)

    def __init__(self, comment: str):
        self.uuid = str(uuid.uuid4())
        self.comment = comment

    def __repr__(self):
        return f"CommentsModel(comment={self.comment})"

    def __str__(self):
        return f"id: {self.uuid}\ncomment: {self.comment}"
