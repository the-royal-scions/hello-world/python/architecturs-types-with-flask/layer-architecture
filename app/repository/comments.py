from typing import Type, List
from app.extensions.database import db
from app.entity.comments import CommentsModel
from .interface import RepositoryInterface


class CommentsRepository(RepositoryInterface):

    def __init__(self):
        self._model = None
        self.model = CommentsModel

    @property
    def model(self) -> CommentsModel:
        return self._model

    @model.setter
    def model(self, model: CommentsModel) -> None:
        if not issubclass(model, db.Model):
            raise ValueError("Not a valide input")
        else:
            self._model = model

    def create(self, **kwargs) -> 'self._model':
        instance = self._model(**kwargs)
        return self.save(instance)

    def get_all(self) -> List['self._model']:
        return self._model.query.all()

    def filter_by(self, **kwargs) -> 'self._model':
        return self._model.query.filter_by(**kwargs).first()

    def update(self, instance: 'self._model', commit=True, **kwargs) -> 'self._model':
        for attr, value in kwargs.items():
            setattr(instance, attr, value)
        return commit and self.save(instance) or instance

    def save(self, instance: 'self._model', commit=True) -> 'self._model':
        db.session.add(instance)
        if commit:
            db.session.commit()
        return instance

    def delete(self, instance: 'self._model', commit=True) -> None:
        db.session.delete(instance)
        return commit and db.session.commit()
