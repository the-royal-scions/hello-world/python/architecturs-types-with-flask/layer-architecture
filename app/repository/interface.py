import abc
from typing import List
from app.extensions.database import db


class RepositoryInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def __init__(self):
        self._model = None
        self.model: db.Model = None

    @abc.abstractmethod
    def create(self, **kwargs) -> 'self._model':
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def get_all(self) -> List['self._model']:
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def filter_by(self, **kwargs) -> 'self._model':
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def update(self, model_instance: 'self._model', commit=True, **kwargs) -> 'self._model':
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def save(self, model_instance: 'self._model', commit=True) -> 'self._model':
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def delete(self, model_instance: 'self._model', commit=True) -> None:
        return NotImplementedError("Not Implemented")
