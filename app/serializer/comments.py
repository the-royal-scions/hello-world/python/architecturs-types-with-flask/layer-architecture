from app.extensions.serializer import ma
from app.entity.comments import CommentsModel


class CommentsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CommentsModel
