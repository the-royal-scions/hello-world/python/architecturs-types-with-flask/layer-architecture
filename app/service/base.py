from .interface import ServiceInterface
from app.repository.interface import RepositoryInterface
from app.extensions.serializer import ma


class ServiceBase(ServiceInterface):
    def __init__(self, repository: RepositoryInterface, serializer: ma.Schema):
        self._repository = None
        self.repository = repository()
        self._serializer = None
        self.serializer = serializer()

    @property
    def repository(self) -> RepositoryInterface:
        return self._repository

    @repository.setter
    def repository(self, repository: RepositoryInterface) -> None:
        if not isinstance(repository, RepositoryInterface):
            raise ValueError
        else:
            self._repository = repository

    @property
    def serializer(self) -> ma.Schema:
        return self._serializer

    @serializer.setter
    def serializer(self, serializer: ma.Schema) -> None:
        if not isinstance(serializer, ma.Schema):
            raise ValueError
        else:
            self._serializer = serializer
