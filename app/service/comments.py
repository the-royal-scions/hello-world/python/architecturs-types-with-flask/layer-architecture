from typing import List
from app.utils.exceptions import ObjectNotFound, ModelDoesntHaveAttr
from .base import ServiceBase
from app.repository.comments import CommentsRepository
from app.serializer.comments import CommentsSchema


class CommentsService(ServiceBase):
    def __init__(self):
        super().__init__(CommentsRepository, CommentsSchema)

    def create(self, **kwargs) -> dict:
        data = self.serializer.load(kwargs, partial=True)
        instance = self.repository.create(**data)
        return self.serializer.dump(instance)

    def get_all(self) -> List[dict]:
        instances_list = self.repository.get_all()
        return self.serializer.dump(instances_list, many=True)

    def model_has_attr(self, attr: str) -> bool:
        if not hasattr(self.repository.model, attr):
            raise ModelDoesntHaveAttr(self.repository.model, attr)

    def filter_by(self, **kwargs) -> dict:
        for key in kwargs.keys():
            self.model_has_attr(key)

        instance = self.repository.filter_by(**kwargs)
        return self.serializer.dump(instance)

    def update(self, where: dict, **kwargs) -> dict:
        for key in where.keys():
            self.model_has_attr(key)

        data = self.serializer.load(kwargs, partial=True)
        instance = self.repository.filter_by(**where)
        if instance:
            new_instance = self.repository.update(instance, **data)
            return self.serializer.dump(new_instance)
        else:
            raise ObjectNotFound

    def delete(self, where: dict) -> None:
        for key in where.keys():
            self.model_has_attr(key)
        instance = self.repository.filter_by(**where)
        if instance:
            return self.repository.delete(instance)
        else:
            raise ObjectNotFound
