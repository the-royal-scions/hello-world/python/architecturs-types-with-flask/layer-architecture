import abc
from typing import List
from app.repository.interface import RepositoryInterface
from app.extensions.serializer import ma


class ServiceInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def __init__(self, repository: RepositoryInterface, serializer: ma.Schema):
        self._repository = None
        self.repository = repository
        self._serializer = None
        self.serializer = serializer

    @abc.abstractmethod
    def create(self, **kwargs) -> dict:
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def get_all(self) -> List[dict]:
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def filter_by(self, **kwargs) -> dict:
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def update(self, where: dict, **kwargs) -> dict:
        return NotImplementedError("Not Implemented")

    @abc.abstractmethod
    def delete(self, where: dict) -> None:
        return NotImplementedError("Not Implemented")
