from app.extensions.database import db
from app.service.comments import CommentsService
from app.repository.comments import CommentsRepository
from app.entity.comments import CommentsModel
from app.serializer.comments import CommentsSchema


def register_shell_context(app):
    def shell_context():
        return {"CommentsModel": CommentsModel,
                "CommentsSchema": CommentsSchema,
                "CommentsService": CommentsService,
                "CommentsRepository": CommentsRepository,
                "db": db}

    app.shell_context_processor(shell_context)
