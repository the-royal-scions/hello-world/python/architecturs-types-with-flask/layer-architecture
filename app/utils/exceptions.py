class ObjectNotFound(Exception):
    def __init__(self, message="Object not found on database."):
        self.message = message
        super().__init__(self.message)


class ModelDoesntHaveAttr(Exception):
    def __init__(self, model: object, attr: str):
        self.model = model
        self.attr = attr
        self.message = f"The model '{self.model.__name__}' doesn't have this attribute '{self.attr}'."
        super().__init__(self.message)
