from flask import abort
from .page_not_found import page_not_found
from .internal_server_error import internal_server_error


def handle_unmatchable(*args, **kwargs):
    return abort(404)


def register_error_handler(bp):
    bp.add_url_rule("/<path:invalid_path>", view_func=handle_unmatchable)
    bp.register_error_handler(404, page_not_found)
    bp.register_error_handler(500, internal_server_error)
