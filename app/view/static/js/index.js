function createRow(comment, uuid) {
    let row = $("<div>").addClass("row").attr("uuid", uuid)

    let col1 = $("<div>").addClass("col-10").attr("contenteditable", "true")
    col1.text(comment)

    let col2 = $("<div>").addClass("col-1")
    let check = $("<a>").html(`
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16">
      <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/>
    </svg>
    `)
    check.on("click", event => {
        event.preventDefault()
        comment = row.children()[0].innerText
        updateComment(uuid, comment)
        location.reload()
    })
    col2.append(check)


    let col3 = $("<div>").addClass("col-1")
    let trash = $("<a>").html(`
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
      <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
      <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
    </svg>
    `)
    trash.on("click", event => {
        event.preventDefault()
        deleteComment(uuid)
        location.reload()
    })
    col3.append(trash)

    row.append([col1, col2, col3])

    let comments = $("#comments")

    comments.append(row)
    let hr = $("<hr>")
    comments.append(hr)

}

async function getComments() {
    const response = await axios.get("/api/comments")
    const data = response.data
    return data
}

commentsPromise = getComments()
commentsPromise.then(data => {
    data.forEach(comment => {
        createRow(comment.comment, comment.uuid)
    })
})

function deleteComment(uuid) {
    axios.delete(`/api/comment?uuid=${uuid}`)
}


function updateComment(uuid, comment) {
    let loginUrl = "/api/comment"
    let data = {
        payload: {
            "comment": comment
        },
        where: {
            "uuid": uuid
        }
    }
    const options = {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        data: data,
        url: loginUrl,
    }
    axios(options)
        .then(response => {
            // console.log("OK")
        }).catch(error => {
            $("#error-message").html("Invalid Credential")
            if (error.response) {
                // console.log(error.response.status)
                // console.log(error.response.statusText)
            } else if (error.request) {
                console.log(error.request)
            } else {
                console.log("Error", error.message)
            }

        })
}

function postComment(comment) {
    let loginUrl = "/api/comment"
    let data = {
        "comment": comment
    }
    const options = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        data: data,
        url: loginUrl,
    }
    axios(options)
        .then(response => {
            // console.log("OK")
        }).catch(error => {
            $("#error-message").html("Invalid Credential")
            if (error.response) {
                // console.log(error.response.status)
                // console.log(error.response.statusText)
            } else if (error.request) {
                console.log(error.request)
            } else {
                console.log("Error", error.message)
            }

        })
}

$("#make-comment-button").on("click", event => {
    event.preventDefault()
    let comment = $("#make-comment").val()
    postComment(comment)
    $("#make-comment").val("")
    location.reload()
})