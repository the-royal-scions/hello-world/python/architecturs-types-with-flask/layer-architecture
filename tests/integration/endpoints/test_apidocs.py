import unittest
from app import create_app


class ApiDocsTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- ApiDocs Endpoint ('/apidocs/') Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.app = create_app("test")
        self.app_client = self.app.test_client()
        self.app_client.testing = True
        self.response = self.app_client.get("/apidocs/")

    def test_get(self):
        print("Testing get status code")
        self.assertEqual(200, self.response.status_code)

    def test_response_content_type(self):
        print("Testing get content type")
        self.assertIn("text/html", self.response.content_type)

    def test_response_content(self):
        print("Checking for swagger-ui")
        response_str = self.response.data.decode("utf-8")
        self.assertIn("swagger-ui", str(response_str))

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Index Endpoint Test Case Finished ----------")
        print("#######################################################")
        pass
