import unittest
from app import create_app
from app.service.comments import CommentsService


class APICommentTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comment Endpoint ('/api/comment') Test Case Started ----------")
        cls.app = create_app("test")
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.app.db.create_all()
        cls.app_client = cls.app.test_client()
        cls.app_client.testing = True

        cls.comments_service = CommentsService()

        cls.comment_1 = {"comment": "First comment"}
        cls.comment_2 = {"comment": "Secound comment"}

        cls.comments_service.create(**cls.comment_1)
        cls.comments_service.create(**cls.comment_2)

        cls.comment_to_delete = {"comment": "Test Delete"}

        cls.comments_service.create(**cls.comment_to_delete)

    def setUp(self):
        print("\n")
        pass

    def test_get_method(self):
        print("Testing get method")
        comment = self.comments_service.filter_by(**self.comment_1)
        url = "/api/comment"
        uuid = comment["uuid"]
        query_string = {"uuid": uuid}
        self.response = self.app_client.get(url, query_string=query_string)

        self.assertEqual(200, self.response.status_code)

        self.assertIn("application/json", self.response.content_type)

        response_json = self.response.get_json()

        resqueted_comment = self.comments_service.filter_by(**response_json)

        self.assertEqual(resqueted_comment["uuid"], comment["uuid"])
        self.assertEqual(resqueted_comment["comment"], comment["comment"])

    def test_post_method(self):
        print("Testing post method")
        url = "/api/comment"
        json = {"comment": "New Test Comment"}
        self.response = self.app_client.post(url, json=json)

        self.assertEqual(201, self.response.status_code)

        self.assertIn("application/json", self.response.content_type)

        response_json = self.response.get_json()

        comment = self.comments_service.filter_by(**response_json)
        self.assertTrue(comment)

        self.comments_service.delete(where=comment)
        comment = self.comments_service.filter_by(uuid=comment["uuid"])
        self.assertFalse(comment)

    def test_delete_method(self):
        print("Testing delete method")
        comment = self.comments_service.filter_by(
            **self.comment_to_delete)
        url = "/api/comment"
        uuid = comment["uuid"]
        query_string = {"uuid": uuid}
        self.response = self.app_client.delete(url, query_string=query_string)

        self.assertEqual(200, self.response.status_code)

        self.assertIn("application/json", self.response.content_type)

        comment = self.comments_service.filter_by(uuid=uuid)
        self.assertFalse(comment)

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- CommentsEndpoint Test Case Finished ----------")
        print("#######################################################")
        pass
