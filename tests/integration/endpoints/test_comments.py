import unittest
from app import create_app
from app.repository.comments import CommentsRepository


@classmethod
def setUpClass(cls):
    print("\n")
    print("#######################################################")
    print("---------- Comment Endpoint ('/api/comment') Test Case Started ----------")


class APICommentsTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comments Endpoint ('/api/comments') Test Case Started ----------")
        cls.app = create_app("test")
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.app.db.create_all()
        cls.app_client = cls.app.test_client()
        cls.app_client.testing = True

    def setUp(self):
        print("\n")
        self.response = self.app_client.get("/api/comments")
        self.response_json = self.response.get_json()

        self.comment_repository = CommentsRepository()

        self.comment_1 = {"comment": "First comment"}
        self.comment_2 = {"comment": "Secound comment"}

        comment = self.comment_repository.create(**self.comment_1)
        comment = self.comment_repository.create(**self.comment_2)

    def test_get(self):
        print("Testing get status code")
        self.assertEqual(200, self.response.status_code)

    def test_response_content_type(self):
        print("Testing get content type")
        self.assertIn("application/json", self.response.content_type)

    def test_response_content(self):
        print("Testing get content")
        self.assertTrue(isinstance(self.response_json, list))

        self.assertEqual(len(self.response_json), 2)

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Comments Endpoint Test Case Finished ----------")
        print("#######################################################")
        pass
