import re
import unittest
from app import create_app
from app.extensions.database import db
from app.service.comments import CommentsService


class CommentsServiceTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comments Repository Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        self.comments_service = CommentsService()

        self.comment_1 = {"comment": "First comment"}
        self.comment_2 = {"comment": "Secound comment"}

    def test_service_create(self):
        print("Testing create function of comment repository")

        comment = self.comments_service.create(**self.comment_1)
        all_comments = self.comments_service.get_all()
        new_comment = all_comments[0]
        self.assertEqual(len(all_comments), 1)

        self.assertTrue(new_comment == comment)

        self.comments_service.create(**self.comment_2)
        all_comments = self.comments_service.get_all()
        self.assertEqual(len(all_comments), 2)

    def test_service_delete(self):

        comment_to_delete = {"comment": "To be deleted"}

        self.comments_service.create(**comment_to_delete)
        self.comments_service.delete(where=comment_to_delete)

        deleted_coment = self.comments_service.filter_by(**comment_to_delete)
        self.assertFalse(deleted_coment)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Comments Repository Test Case Finished ----------")
        print("#######################################################")
        pass
