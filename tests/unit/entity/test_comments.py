import re
import unittest
from app import create_app
from app.extensions.database import db
from app.entity.comments import CommentsModel


class CommentsModelTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comments Model Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.data = {"comment": "Test Coment"}
        self.comment = CommentsModel(**self.data)

    def test_comment(self):
        print("Testing comment attribute")
        self.assertEqual(
            self.comment.comment, self.data["comment"]
        )

    def test_uuid4(self):
        print("Testing uuid attribute")
        self.assertEqual(len(self.comment.uuid), 36)
        matched = re.match(
            r"\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b",
            self.comment.uuid)
        self.assertTrue(bool(matched))

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Comments Model Test Case Finished ----------")
        print("#######################################################")
        pass
