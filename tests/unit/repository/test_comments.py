import re
import unittest
from app import create_app
from app.extensions.database import db
from app.repository.comments import CommentsRepository


class CommentsRepositoryTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comments Repository Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        self.comment_repository = CommentsRepository()

        self.comment_1 = {"comment": "First comment"}
        self.comment_2 = {"comment": "Secound comment"}

    def test_repository_create(self):
        print("Testing create function of comment repository")

        comment = self.comment_repository.create(**self.comment_1)
        all_comments = self.comment_repository.get_all()
        new_comment = all_comments[0]
        self.assertEqual(len(all_comments), 1)

        self.assertTrue(new_comment == comment)

        self.comment_repository.create(**self.comment_2)
        all_comments = self.comment_repository.get_all()
        self.assertEqual(len(all_comments), 2)

    def test_repository_delete(self):

        comment_to_delete = {"comment": "To be deleted"}

        comment = self.comment_repository.create(**comment_to_delete)
        self.comment_repository.delete(comment)

        deleted_coment = self.comment_repository.filter_by(**comment_to_delete)
        self.assertFalse(deleted_coment)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Comments Repository Test Case Finished ----------")
        print("#######################################################")
        pass
